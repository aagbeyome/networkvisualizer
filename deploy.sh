#!/bin/bash
cd web-app
npm run build
cd ..
docker build --build-arg PORT1=8090 -t aagbeyome/networkvisualizer .
docker save -o networkvisualizer.tar aagbeyome/networkvisualizer 
scp networkvisualizer.tar anthony@kynetic-systems.com:/home/anthony
rm -f networkvisualizer.tar 
exit