(()=>{
    const express = require('express');
    const path = require('path');
    const cors = require("cors");

    const port = process.env["PORT"] || 8090;
    const app = express();

    app.use(cors());
    app.use(express.static(path.join(__dirname, 'web-app/build')));
    const server = require('http').Server(app);

    app.get('/', async (req, res) => {
        res.sendFile(path.join(__dirname, 'web-app/build', 'index.html'));
    });

    app.get('/data', async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        
        res.send(JSON.stringify({ "vertices": [{  "id": "n1",  "label": "Node 1", "type": "node"},
            {"id": "n2", "label": "Node 2", "type": "node"},
            {"id": "a1", "label": "Alarm 1",  "type": "alarm"}],
            "edges": [{"id": "e1", "label": "edge n1-n2","type": "link","source_id": "n1", "target_id": "n2" },
            { "id": "e2", "label": "edge n2-a1", "type": "link", "source_id": "n2","target_id": "a1"} ]
        }));
    });

    server.listen(port, () => {
        console.log(`webserver started on port : ${port}..`);
    })
    .on("error", (err) => {
        console.log(`unable to bind to port : ${port} \n ${err}`);
    });

})()