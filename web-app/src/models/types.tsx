
export interface IVertex{
    id: string;
   label: string;
   type: string;
};

export interface IEdge{
    id: string;
    label: string;
    type: string;
    source_id: string;
    target_id: string
};

export interface GraphData{
    vertices?: IVertex[];
    edges?: IEdge[]
};

export interface AppState{
    graphData: GraphData
};

export interface IAction{
    payload: any;
    type: string;
};

export interface ICyElement{
    data: ICyNode;
};

export interface ICyNode{
    id: string;
    label: string;
    type: string;
    source?: string;
    target?: string;
    name?: string
}