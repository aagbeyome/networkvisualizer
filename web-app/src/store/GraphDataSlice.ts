import { createSlice, createAsyncThunk, PayloadAction} from '@reduxjs/toolkit';
import { DataService } from '../services/DataService';
import { AppState, GraphData } from '../models/types';

export const fetchInitData = createAsyncThunk(
    'graphData/fetchInitialData',
    async () => {
        let res:GraphData = {};
        try{
            const dataservice = new DataService();
            const resp = await dataservice.fetchData();
            
            res = resp.data;
        }catch(err){
            //console.log(err)
        }
      return res;
    }
  ) 

const initialState = {
  graphData: {}
} as AppState

export const graphDataSlice = createSlice({
  name: 'graphData',
  initialState,
  reducers: {
    setData: (state, action) => {
      state.graphData = {...action.payload}
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchInitData.fulfilled, (state, action: PayloadAction<any> ) => {
        state.graphData = action.payload;
    })
  },
});

export const { setData } = graphDataSlice.actions

export default graphDataSlice.reducer