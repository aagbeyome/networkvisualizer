import { configureStore } from '@reduxjs/toolkit'
import {graphDataSlice} from './GraphDataSlice'

export default configureStore({
  reducer: {
   graphData: graphDataSlice.reducer
  },
})