import axios, {AxiosResponse} from 'axios';

class DataService {
    
    readonly serverUrl: String =  `${window.location.protocol}//${window.location.hostname}`;
    readonly dataEndpoint: String = "/data";
    readonly port: string = (window.location.port === "3000") ? "8090" : window.location.port

    fetchData(): Promise<AxiosResponse>{
        const url = `${this.serverUrl}:${this.port}${this.dataEndpoint}`
        return axios.get(url);
    }
}

export { DataService }