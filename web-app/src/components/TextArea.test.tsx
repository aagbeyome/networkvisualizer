import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import TextArea from './TextArea';
 
const mockStore = configureStore([]);

const mockData = {"graphData" : { "graphData" : { "vertices": [{  "id": "n1",  "label": "Node 1", "type": "node"},
    {"id": "n2", "label": "Node 2", "type": "node"},
    {"id": "a1", "label": "Alarm 1",  "type": "alarm"}],
    "edges": [{"id": "e1", "label": "edge n1-n2","type": "link","source_id": "n1", "target_id": "n2" },
    { "id": "e2", "label": "edge n2-a1", "type": "link", "source_id": "n2","target_id": "a1"} ]
  } }}
 
describe('TextArea React-Redux Component', () => {
  let store:any;
  let component:any;
 
  beforeEach(() => {
    store = mockStore({ "textVal": JSON.stringify(mockData), isValidJson: true });
    store.dispatch = jest.fn();

    component = renderer.create(
      <Provider store={store}>
        <TextArea />
      </Provider>
    );
  });
 
  it('should render with given state from Redux store', () => {
    expect(component.toJSON()).toMatchSnapshot();
  });
 
  it('should dispatch an action and check state update', () => {
    renderer.act(() => {
      component.root.findByType('textarea')
        .props.onChange({ target: { value: JSON.stringify(mockData) } });
    });

    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

});