import React, { useEffect } from 'react';
import './Graph.scss';
import { useSelector } from 'react-redux';
import { ICyElement, IEdge, IVertex } from '../models/types';

declare const cytoscape: any;

export default function Graph() {

  //fetch data from store
  const storeData = useSelector((state) => state)

  const drawGraph = () => {
    try{
      let graphElements: ICyElement[] = [];

      const getIcon = (type:string): string=>{
        return (type === "link") ? "./imgs/link.png" : ( (type === "alarm") ? "./imgs/alarm.png" : "./imgs/node.png" )
      }
  
      let styleObj = cytoscape.stylesheet()
                        .selector('node')
                        .css({  'height': 80, 'width': 80, 'background-fit': 'cover', 'border-color': '#000', 'border-width': 3, 'border-opacity': 0.5, label: 'data(label)' })
                        .selector('edge')
                        .css({ 'curve-style': 'bezier', 'width': 6, 'line-color': '#0073b0', 'target-arrow-color': '#0073b0', shape: 'square', label: 'data(label)'})         
  
      const addIcon = (type: string, id: string) =>{
        styleObj = styleObj.selector(`#${id}`).css({  'background-image': getIcon(type)  })
      }
  
      if(storeData){
       
        const data:any = storeData;

        const graphData = data.graphData.graphData;
        const {vertices, edges} = graphData;
  
        if(Array.isArray(vertices) && Array.isArray(edges)){
          vertices.forEach( (item: IVertex) =>{
            addIcon(item.type, item.id)
            graphElements.push({"data" : {  "id" : item.id, "label" : item.label, "type" : item.type }})
          })
  
          edges.forEach( (edge: IEdge) =>{
            graphElements.push({"data" : {  "id" : edge.source_id+""+edge.target_id, "label" : edge.label, 
                                        "type" : edge.type, "source" : edge.source_id, "target" : edge.target_id } })
          })
        }
  
        cytoscape({
          container: document.getElementById('vizGraph'),
          elements: graphElements,
          layout: {  directed: true, padding: 10 },
          style: styleObj  
        });
  
      }
    }catch(ex){

    }
  }

  useEffect(() => {
    drawGraph();
  }, [storeData]);


  return (
    <div className="col-md-6 col-lg-6 col-xl-6 col-xs-12 col-sm-12 mx-height">
      <div className="card shadow-sm">
        <div className="card-body">
          <div className="mb-3">
            <label className="form-label">Network Graph</label>
            <div id="vizGraph"></div>
          </div>
        </div>
      </div>
    </div>
  );
}
