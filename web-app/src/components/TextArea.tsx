import React, { useEffect, useReducer } from 'react';
import './TextArea.scss';
import { useSelector, useDispatch } from 'react-redux';
import { IAction } from '../models/types';
import { setData } from '../store/GraphDataSlice';

export default function TextArea() {

    let initState = { "textVal": "", isValidJson: true }

    const updateStateReducer = (state: any, action: IAction) => {

        try {
            // console.info(action.type, action.payload)
            switch (action.type) {
                case 'SHOW_DATA':
                    let ddata = action.payload.graphData.graphData;
                    ddata = (typeof ddata === "string") ? ddata : JSON.stringify(ddata, null, 2)
                    return { ...state, "textVal": ddata }

                case 'DATA_EDITED':
                    const { textVal, isValidJson } = action.payload
                    return { ...state, textVal, isValidJson }

                default:
                    throw new Error();
            }
        } catch (ex) {
            return {...state}
        }
    }

    const [localState, dispatchReducer] = useReducer(updateStateReducer, initState);

    //fetch data from store
    const storeState = useSelector((state) => state)
    const storeDispatch = useDispatch();

    useEffect(() => {
        dispatchReducer({ type: 'SHOW_DATA', payload: storeState })
    }, [storeState, dispatchReducer]);

    const handleUserEdit = (textstr: string) => {
        let isValid = false
        try {
            const newJson = JSON.parse(textstr); isValid = true;

            //dispatch new json to store
            storeDispatch(setData(newJson));
        } catch (ex) { }
        dispatchReducer({ type: 'DATA_EDITED', payload: { "textVal": textstr, "isValidJson": isValid } })
    }

    let isInvalidJson = <div />
    if (localState.isValidJson === false) {
        isInvalidJson = (<label className="err-label">Please provide valid JSON</label>)
    }

    //render
    return (
        <div className="col-md-6 col-lg-6 col-xl-6 col-xs-12 col-sm-12 mx-height">
            <div className="card shadow-sm">
                <div className="card-body">
                    <div className="mb-3">
                        <label className="form-label">Paste JSON</label>
                        <textarea value={localState.textVal} className="form-control" rows={20} onChange={(e) => {
                            handleUserEdit(e.target.value)
                        }} />
                        {isInvalidJson}
                    </div>
                </div>
            </div>
        </div>
    );
}
