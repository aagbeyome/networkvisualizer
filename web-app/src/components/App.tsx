import React, {useEffect} from 'react';
import { useDispatch } from 'react-redux';
import { fetchInitData } from '../store/GraphDataSlice';
import './App.scss'; 

import Graph from './Graph';
import TextArea from './TextArea';

export default function App() {
  const storeDispatch = useDispatch();

  useEffect(() => {
    storeDispatch(fetchInitData());
  }, [storeDispatch]);

  return (
    <div className="row">
        <TextArea />
        <Graph />
    </div>
  );
}

 
