# NetworkVisualizer
### Stack
```
This demo project was built with React, Bootstrap and Cytoscape.js. App is made up of 3 components and uses Redux, Redux Toolkit and Axios. A minimal ExpressJS server is included to serve the initial Graph data. A Docker build file and script have been included for deployment as a Docker container
```

### How to Build and run
```
1) Clone the project

2) In root directory run 
   $ npm install   # this is for Exppress Server. 
   $ npm start    # to start the local server on 8090 that will serve the initial graph data

3) Switch to React project folder /web-app and run 
   $ npm install
   $ npm run start   #to start React Dev server on port 3000 and launch application in browser
```

### Prod Builds
```
1) in /web-app folder run npm run build

2) step out into root directory and run npm run start to start express server on port 8090

3) Access page on http://localhost:8090/
```

### Deploy with Docker
```sh
./deploy.sh
```


### Live Demo
   
  http://www.kynetic-systems.com:8090/
   